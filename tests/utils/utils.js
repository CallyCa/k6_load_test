import env from '../utils/config_env'

const url = env.URL_TEST.SERVREST

export default class Utils {
	static getBaseUrl() {
		switch (process.env.NODE_ENV) {
			case 'production':
				return url
			default:
				console.error('Url or env not defined')
		}
	}
}
