// Here you to set BASE URL

module.exports = {
	URL_TEST: {
		SERVREST: 'https://serverest.dev',
	},
	BASES_URL: {
		BASE_URL_PPI:
			'http://PIX:PIX123@10.232.0.174:8080/partner-interface-oauth2/oauth/token',
		BASE_URL_BANK_API:
			'https://qa-bank-api.hubfintech.com.br/prepaid/api/oauth/issue',
	},
	BASE_USERS: {
		USERNAME_USER: '46212128006',
	},
	BASE_PASSWORDS: {
		PASSWORD:
			'4a9483cec71a6fc175d7f0874499bbc413b780899a87edbd67c68bde23e9e163',
	},
	CLIENTS: {
		CLIENT_ID: 'HUBBANK',
	},
}
