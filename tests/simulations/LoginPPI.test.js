import Login from '../requests/loginppi.request.js'

import { group } from 'k6'

export let options = {
	stages: [
		// Simula o aumento do tráfego de 1 para 20 usuários em 10 segundos.
		{ duration: '30s', target: 1 },

		// Permanecer em 100 usuários por 10 minutos

		{ duration: '1m', target: 5 },

		// Redução para 0 usuários

		{ duration: '30s', target: 0 },
	],
	thresholds: {
		// 99% das solicitações devem ser concluídas abaixo de 1,5 s

		http_req_duration: ['p(99)<1500'],
	},
}

export default function () {
	let login = new Login()

	group('Access with admin user', () => {
		login.access()
	})
}
