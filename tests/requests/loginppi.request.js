import { check } from 'k6'
import http from 'k6/http'
import env from '../utils/config_env'
// import dotenv from 'dotenv';

const url = env.BASES_URL.BASE_URL_PPI

export default class LoginPPI {
	constructor() {
		this.params = {
			headers: {
				accept: 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded',
				monitor: false,
			},
		}
		this.token = ''
	}

	access() {
		let body = JSON.stringify({
			grant_type: 'client_credentials',
		})

		let response = http.post(url, body, this.params)
		this.token = response.json('authorization')
		check(response, {
			'is status 200': () => response.status === 200,
		})
	}

	getToken() {
		return this.token
	}
}
