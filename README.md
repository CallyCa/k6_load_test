# Load Testing


## Ferramentas utilizadas:  
- [JS](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript) 
- [npm](https://www.npmjs.com/)
- [k6](https://k6.io/)
- [docker](https://www.docker.com/get-started)
- [Grafana](https://grafana.com/)
- [Influxdb](https://github.com/influxdata/influxdb)
- [Python](https://www.python.org/downloads/)
- [Ansible](https://docs.ansible.com/ansible/latest/user_guide/index.html#getting-started)


### Funcionamento:

O `k6` permite que montemos diversos cenários para realização de testes de carga para vermos como um determinado serviço se comporta ao receber diversas requisições.

Todos os dados gerados pelo `k6` são entregues em uma base de dados de série temporal, no nosso caso o `influxdb`.

O `grafana` lê todos os dados dispostos no `influxdb` e exibe os resultados em um dashboard.


### Pré-requisitos: 
- Instalação [**k6**](https://k6.io/docs/getting-started/installation/)
- Instalação [**docker/docker-compose**](https://www.docker.com/get-started)


## Instalação do Projeto:

```bash
$ yarn 
ou 
$ yarn install
```

Docker:
```bash
$ sudo docker-compose up
```

Executar Testes no Docker:
```bash
$ yarn docker:FullFlowLoadTest
ou
$ yarn docker:FullFlowStressTest
```

Executar lint para correção de erros no código:
```bash
$ yarn lint
```

### Dashboards Grafana:

O dashboard estará sendo executado na seguinte URL: http://localhost:3000/

## Obs:

A base de dados para a realização dos testes de performance desse projeto está sendo coletada desta URL: https://serverest.dev

## ServeRest

O ServeRest é uma API REST que simula uma loja virtual com intuito de servir de material de estudos de testes de API.

Repositório: https://github.com/ServeRest/ServeRest

## Ansible

`Ansible` é uma ferramenta escrita em Python para automatização, provisionamento, gerenciamento de configurações e orquestração de serviços. Podendo ser capaz de ser utilizada em conjunto com outras ferramentas de orquestração de serviços como Terraform, por exemplo.

Ele utiliza por padrão o protocolo SSH para se conectar aos servidores e executar as tarefas escritas em um arquivo `.yml`, também sendo nomeado Playbook.


1. Instalar Ansible:
```bash
sudo apt update && sudo apt install ansible unzip git -y
```

2. Executar a configuração estabelecida no Playbook:
```bash
ansible-playbook ansible_tools/ubuntu/ubuntu_tools.yml --ask-become-pass
```
>Sua senha root será exigida para determinadas tarefas em execução no Playbook.
___