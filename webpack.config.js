// const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");

module.exports = {
	mode: 'production',
	entry: {
		LoginPPI: './tests/simulations/LoginPPI.test.js',
		FullFlowLoad: './tests/simulations/FullFlowLoad.test.js',
		FullFlowStress: './tests/simulations/FullFlowStress.test.js',
	},
	output: {
		path: __dirname + '/dist',
		filename: '[name].test.js',
		libraryTarget: 'commonjs',
	},
	module: {
		rules: [{ test: /\.js$/, use: 'babel-loader' }],
	},
	stats: {
		colors: true,
		warnings: false,
	},

	//   plugins: [new NodePolyfillPlugin()],

	// resolve: {
	//   alias: {
	//     path: require.resolve("path-browserify"),
	//     },
	// },
	// var config = Encore.getWebpackConfig()
	// config.node = { fs: 'empty' }
	// module.exports = config

	target: 'web',
	externals: /k6(\/.*)?/,
	devtool: 'source-map',
}
