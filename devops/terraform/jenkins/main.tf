provider "aws" {
    region = "us-east-1"
}

variable "name" {
    description = "Name the instance on deploy"
}

resource "aws_instance" "devops_01" {
    ami = "ami-04505e74c0741db8d"
    instance_type = "t2.large"
    key_name = "calendario2009"

    tags = {
        Name = "${var.name}"
    }
}
