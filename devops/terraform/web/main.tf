provider "aws" {
    region = "us-east-1"
}

variable "name" {
    description = "Name of the web box at apply"
}

resource "aws_instance" "do_web_01" {
    ami = "ami-04505e74c0741db8d"
    instance_type = "t2.large"
    key_name = "devops_01"

    tags = {
        Name = "${var.name}"
    }
}